//based on feildmaster's localchat
package com.lx75249.localchatplus;
//TODO 配置中加入 ----1.消息前缀 ---2.喊话花钱~ 3.刷屏提示
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.massivecraft.factions.P;

public final class Chat extends JavaPlugin 
{
    private final ChatListener listener = new ChatListener(this);
    private Map<String, String> cache = new HashMap<String, String>();
    private Map<String, String> LastMessage = new HashMap<String, String>();
    private int range = 1000;
    private P factions;
    private Economy economy;

    public void onLoad() 
    {
        File file = new File(this.getDataFolder(), "config.yml");
        if (!file.exists() || !configContainsDefaults()) 
        {
            getConfig().options().copyDefaults(true);
            try 
            {
                getConfig().save(file);
            }
            catch (IOException ex) 
            {
            	getLogger().severe("LocalChat插件 默认配置文件保存出错");
            	getServer().getPluginManager().disablePlugin(this);
            	return ;
            }
        }

        loadConfig();
    }

    public void onEnable() 
    {
    	if (!initEconomy())
    	{
    		getLogger().severe("聊天插件启动失败");
    		getServer().getPluginManager().disablePlugin(this);
    		return;
    	}
        Plugin p = getServer().getPluginManager().getPlugin("Factions");
        if (p instanceof P) 
        {
            factions = (P) p;
        } 
        else
        	if (p != null) 
        	{
        		getLogger().warning("加载公会插件时发生错误(可能是不支持的版本)");
        	}

        getServer().getPluginManager().registerEvents(listener, this);
    }

    private void loadConfig() 
    {
        range = (int) Math.pow(getConfig().getInt("local.range"), 2);
    }

    public int getLocalRange() 
    {
        return range;
    }

    public String getPrefix(String type) 
    {
        return getConfigString(type+".prefix");
    }

    public String getFormat(String type) 
    {
        return getConfigString(type+".format");
    }

    public String getGlobalPermissionMessage() 
    {
        return getConfigString("global.no_permission");
    }

    public String getLocalNullMessage() 
    {
        return getConfigString("local.null_message");
    }

    public String getLackMoneyMessage()
    {
    	return getConfigString("shout.lack-money");
    }
    
    public String getRepeatMessage()
    {
    	return getConfigString("repeat");
    }
    
    public int getShoutCost()
    {
    	return Integer.parseInt(getConfigString("shout.cost"));
    }
    
    public String getMessageFormat(String type)
    //获取消息的格式
    {
    	return getConfigString(type+".format");
    }
    
    public String getLastMessage(String PlayerName)
    //获得用户上一条消息
    {
    	return LastMessage.get(PlayerName);
    }

    private String getConfigString(String name) 
    {
        String value = cache.get(name);
        if (value != null) 
        {
            return value;
        }

        value = getConfig().getString(name);
        cache.put(name, value);
        return value;
    }

    public P getFactionsPlugin() 
    {
        return factions;
    }

    private boolean configContainsDefaults()
    {
        if (getConfig().getDefaults() == null) 
        {
            return true;
        }
        return getConfig().getKeys(true).containsAll(getConfig().getDefaults().getKeys(true));
    }
    
    private boolean initEconomy()
    {
        if (getServer().getPluginManager().getPlugin("Vault") == null)
        {
        	System.out.println("\n找不到Vault插件");
        	return false;
        }
        RegisteredServiceProvider<Economy> EconomyProvider = getServer().getServicesManager().getRegistration(Economy.class);

        if (EconomyProvider == null)
        {
        	System.out.println("\n找不到经济插件");
        	return false;
        }

        economy = ((Economy)EconomyProvider.getProvider());

        return economy != null;
    }
    
    public boolean chargePlayer(Player player, int money)
    //收钱
    {
    	//player.sendMessage("当前余额"+economy.getBalance(player.getName()));
    	if (economy.getBalance(player.getName()) < (double)money)
    	{
    		return false;
    	}
    	economy.withdrawPlayer(player.getName(), (double)money);
    	return true;
    }
    

    public void saveMessage(Player player, String msg)
    //保存玩家的最后一条消息 用于检测刷屏
    {
    	LastMessage.put(player.getName(), msg);
    }
    
}
