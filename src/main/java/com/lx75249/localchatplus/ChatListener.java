//based on feildmaster's localchat
package com.lx75249.localchatplus;

import java.util.HashSet;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener
{
    private Chat plugin;
    private Exception badMessage = new Exception();

    public ChatListener(Chat p)
    {
        plugin = p;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerChat(AsyncPlayerChatEvent event)
    {
    	Player player = event.getPlayer();
    	
        if (plugin.getFactionsPlugin() != null && plugin.getFactionsPlugin().isPlayerFactionChatting(player))
        // 跟公会插件共存
        {
            return;
        }
        
        try
        {
     		if (event.getMessage().startsWith(plugin.getPrefix("shout")))
    		//喊话 , 不受刷屏限制
    		{
    			if (plugin.chargePlayer(player, plugin.getShoutCost()))
    			{
    				//收费成功
    				String msg = event.getMessage().substring(plugin.getPrefix("shout").length());
    				plugin.getServer().broadcastMessage
    				(
    					plugin.getFormat("shout")
    						.replace("%1$s", player.getName())
    						.replace("%2$s", msg)
    				);
    			}
    			else
    			{
    				//钱不够
    				player.sendMessage(plugin.getLackMoneyMessage().replace("%money%", String.valueOf(plugin.getShoutCost())));
    			}
    			throw badMessage;
    			//喊话永远不会真正发出消息
    		}
     		
        	if (plugin.getLastMessage(player.getName()) != null 
        	 && plugin.getLastMessage(player.getName()).equalsIgnoreCase(event.getMessage()))
        	{
        		//刷屏检测 区分大小写
        		player.sendMessage(plugin.getRepeatMessage());
        		throw badMessage;
        	}      
        	plugin.saveMessage(player, event.getMessage());
        	
        	
        	if (event.getMessage().startsWith(plugin.getPrefix("global"))) 
        	{
        		//世界频道
        		if (!player.hasPermission("localchat.global")) 
        		{
        			if (plugin.getGlobalPermissionMessage() != null && plugin.getGlobalPermissionMessage().length() > 0) 
        			{
        				player.sendMessage(plugin.getGlobalPermissionMessage());
        			}
        			throw badMessage;
        		}
        		event.setMessage(event.getMessage().substring(plugin.getPrefix("global").length()));
        		event.setFormat(plugin.getFormat("global"));
        		return;
        	}
        	else //本地
        	{
                for (Player r : new HashSet<Player>(event.getRecipients())) 
                {
       	            if (outOfRange(player.getLocation(), r.getLocation()))
       	            {
       	            	event.getRecipients().remove(r);
     		        }
       		    }
                if (event.getRecipients().size() == 1)
                {
                	//本地范围内没人
                	player.sendMessage(plugin.getLocalNullMessage());
                	throw badMessage;
       		    }
                event.setFormat(plugin.getFormat("local"));
        	}
        //try end
        }
        catch(Exception e)
        {
			event.setCancelled(true);
			return;
        }
    }

    private boolean outOfRange(Location l, Location ll)
    {
        if (l.equals(ll)) 
        {
            return false;
        } 
        else
        	if (l.getWorld() != ll.getWorld()) 
        	{
        		return true;
        	}
        return l.distanceSquared(ll) > plugin.getLocalRange();
    }
}
